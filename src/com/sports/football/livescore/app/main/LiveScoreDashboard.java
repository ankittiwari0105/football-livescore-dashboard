package com.sports.football.livescore.app.main;

import com.sports.football.livescore.dashboard.Dashboard;
import com.sports.football.livescore.game.Football;
import com.sports.football.livescore.playgame.PlayGame;

// Main application game to initiate instance
public class LiveScoreDashboard {

	public static void main(String[] args) {
		Football footBall = new Football();
		PlayGame playGame = new PlayGame(footBall);
		
		Dashboard window = new Dashboard(playGame);
		window.setVisible(true);

	}

}
