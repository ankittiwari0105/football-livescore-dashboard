package com.sports.football.livescore.playgame;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sports.football.livescore.game.GameInterface;
import com.sports.football.livescore.user.input.CommandInterface;
import com.sports.football.livescore.user.input.InvalidCommand;
import com.sports.football.livescore.user.input.PrintCommand;
import com.sports.football.livescore.user.input.ScoreCommand;
import com.sports.football.livescore.user.input.StartCommand;
import com.sports.football.livescore.user.input.StopCommand;

public class PlayGame {
	private String awayTeam;
	private String homeTeam;
	
	private String team;
	private int minutes;
	private String player;
	
	private GameInterface game;
	
	/**
	 * Constructor
	 * @param GameInterface game
	 */
	public PlayGame(GameInterface game) {
		this.game = game;
	}
	
	/**
	 * Depending inputs handle the creation of commands.
	 * @param String string
	 * @return CommandInterface command
	 */
	public CommandInterface parse(String string) {
		if(Pattern.matches("^Start: '(.+)' vs. '(.+)'$", string)) {
			Pattern r = Pattern.compile("Start: '(.+)' vs. '(.+)'$"); 
			
			Matcher m = r.matcher(string);
			
			while (m.find()) {
				awayTeam = m.group(1);
				homeTeam = m.group(2);
			}
			
			return new StartCommand(game, this);
		}
		else if(Pattern.matches("^(\\d+) '(.+)' (.+)$", string)) {
			Pattern pattern = Pattern.compile("(\\d+) '(.+)' (.+)");
			Matcher matcher = pattern.matcher(string);
			
			while (matcher.find()) {
				minutes = Integer.parseInt(matcher.group(1));
				team = matcher.group(2);
				player = matcher.group(3);
			}
			
			return new ScoreCommand(game, this);
		}
		else if(string.equals("print")) {
			return new PrintCommand(game);
		}
		else if(string.equals("end")) {
			return new StopCommand(game);
		}
		else
			return new InvalidCommand(game);
	}
	
	public String getAwayTeam() {
		return awayTeam;
	}

	public String getHomeTeam() {
		return homeTeam;
	}

	public String getTeam() {
		return team;
	}

	public int getMinutes() {
		return minutes;
	}

	public String getPlayer() {
		return player;
	}
}
