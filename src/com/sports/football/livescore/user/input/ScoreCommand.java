package com.sports.football.livescore.user.input;

import com.sports.football.livescore.game.GameInterface;
import com.sports.football.livescore.playgame.PlayGame;

/**
 * @author Ankit
 * Manage the scoring of the game.
 * 
 */
public class ScoreCommand implements CommandInterface {
	
	private GameInterface game;
	private PlayGame gameParser;

	public ScoreCommand(GameInterface game,PlayGame gameParser){
	      this.game = game;
	      this.gameParser = gameParser;
	}
	
	@Override
	public String execute() {
		return game.score(gameParser.getMinutes(), gameParser.getTeam(), gameParser.getPlayer());
	}

}
