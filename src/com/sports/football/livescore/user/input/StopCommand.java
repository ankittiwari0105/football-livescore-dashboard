package com.sports.football.livescore.user.input;

import com.sports.football.livescore.game.GameInterface;

/**
 * @author Ankit
 * Finish the current game.
 * 
 */
public class StopCommand implements CommandInterface {
	
	private GameInterface game;

	public StopCommand(GameInterface game){
	      this.game = game;
	}
	
	@Override
	public String execute() {
		return game.end();
	}

}
