package com.sports.football.livescore.user.input;

import com.sports.football.livescore.game.GameInterface;

/**
 * @author Ankit
 * Invalid command on the game.
 * 
 */
public class InvalidCommand implements CommandInterface {
	
	private GameInterface game;

	public InvalidCommand(GameInterface game){
	      this.game = game;
	}
	
	@Override
	public String execute() {
		return game.invalid();
	}

}
