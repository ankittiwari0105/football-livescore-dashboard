package com.sports.football.livescore.user.input;

public interface CommandInterface {
	public String execute();
}
