package com.sports.football.livescore.user.input;

import com.sports.football.livescore.game.GameInterface;
import com.sports.football.livescore.playgame.PlayGame;

/**
 * @author Ankit
 * Start a new game.
 * 
 */
public class StartCommand implements CommandInterface {
	
	private GameInterface game;
	private PlayGame gameParser;
	
	public StartCommand(GameInterface game, PlayGame gameParser){
	      this.game = game;
	      this.gameParser = gameParser;
	}
	
	@Override
	public String execute() {
		return game.start(gameParser.getAwayTeam(), gameParser.getHomeTeam());
	}

}
