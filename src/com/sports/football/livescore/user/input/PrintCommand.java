package com.sports.football.livescore.user.input;

import com.sports.football.livescore.game.GameInterface;

/**
 * @author Ankit
 * Print command for the current game score. 
 * 
 */
public class PrintCommand implements CommandInterface {
	
	private GameInterface game;

	public PrintCommand(GameInterface game){
	      this.game = game;
	}
	
	@Override
	public String execute() {
		return game.print();
	}

}
