package com.sports.football.livescore.game;

// Interface to specify different user actions in game
public interface GameInterface {
	public String start(String awayTeam, String homeTeam);

	public String print();

	public String score(int minute, String team, String player);

	public String end();

	public String invalid();
}
